# README #


### What is this repository for? ###

* This repository contains reproducible R code the master thesis of Georgios Kazantzidis for the master porgramme in Biostatistics at the University of Zurich. 
* A running version of R and LaTeX is required
* The R Package "Collinearity" is required and can be found in https://github.com/G-Kazantzidis/Collinearity.git 

### Packages required ###
* Use the following code to install all the required R packages 
* install.packages(c("xtable", "tidyverse", "ellipse", "RColorBrewer", "ggplot2", "GGally", "plotly", "ggpubr", "foreach", "doParallel", "plot3D", "scales", "latex2exp", "reshape2", "ggridges", "viridis", "hrbrthemes", "shrink", "tableone", "ggrepel", "ggtext", "showtext", "ggforce", "lme4", "ggpmisc"))
* install.packages("biostatUZH", repos="http://R-Forge.R-project.org")
* install.packages("INLA",repos=c(getOption("repos"),INLA="https://inla.r-inla-download.org/R/stable"), dep=TRUE)

### Directly install package Collinearity ###

* install.packages("devtools") 
* library("devtools")
* install.packages("withr") 
* library(withr) 
* install_github("G-Kazantzidis/Collinearity")
 

### How do I get set up? ###

* Download all the files of the repository or clone the repository locally
* Compile the file MSc_Report.Rnw
* (You can use the code knitr::knit2pdf("MSc_Report.Rnw")


### Who do I talk to? ###

* George_Kazantzidis@outlook.com 
* Georgios.kazantzidis@uzh.ch