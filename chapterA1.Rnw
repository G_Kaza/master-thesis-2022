% LaTeX file for Chapter 01
<<'preambleA1',include=FALSE>>=
library(knitr)
opts_chunk$set(
    fig.path='figure/cha1_fig', 
    self.contained=FALSE,
    cache=TRUE
)

@


\chapter{Appendix}

\begin{itemize}
\item \texttt{Collinearity} R package: \url{https://github.com/G-Kazantzidis/Collinearity.git}
\item Reproducible code: \url{https://bitbucket.org/G_Kaza/master-thesis-2022/src/master/}. All the contents of folder 'Final' are required for the compilation of the file. The executable file is 'MSc\_Report.Rnw'. It is located in the folder named 'Final'.
\end{itemize}

<< sessioninfo, results = TRUE >>=
sessionInfo()
@


